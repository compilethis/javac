#!/bin/env python3

import argparse
import sys
import re

from graphviz import Digraph


def getParser():
    parser = argparse.ArgumentParser(
            description='Tool to create a dot file from a jay file'
            )

    parser.add_argument(
            'infile',
            type=argparse.FileType("r"),
            help="jay file to analyze",
            )

    parser.add_argument(
            '--outfile',
            '-o',
            nargs='?',
            type=argparse.FileType("r"),
            default=sys.stdout,
            help="path of the dotfile to create (default: stdout)",
            )

    return parser


parser = getParser()
args = parser.parse_args()
print(args.infile)

pattern = re.compile(
        "(?P<source_rule>[a-zA-Z0-9_]+)?([:|])(?P<dest>[a-zA-Z0-9_ ]+){(.*)$"
        )

dot = Digraph(comment=args.infile.name)

src = ""
for line in args.infile:
    # print(line)
    match = pattern.search(line)
    if match:
        if match["source_rule"]:
            src = match["source_rule"]
            dot.node(src)
        # print(src + " -> " + match["dest"])
        joined_edge = "_".join(match["dest"].split())
        dot.node(joined_edge)
        dot.edge(src, joined_edge)
        for part in match["dest"].split():
            dot.node(part)
            dot.edge(joined_edge, part)


print(dot.source)

dot.format = "jpg"

dot.render(args.infile.name + ".dot" )
