# Configuration
JAY_FLAGS ?= -v -t
JAY = $(PWD)/bin/jaooy.linux

JAVAC_FLAGS ?= -encoding utf8
JAVAC = javac $(JAVAC_FLAGS) -cp $(CLASSPATH)

JAVA_FLAGS ?=
JAVA = java $(JAVA_FLAGS) -cp $(CLASSPATH)

JCLASSLIB = ./cp

BUILDPATH = ./build

SKELETON = skeleton.jaooy
CLASSPATH = .:$(BUILDPATH)/MAIN:$(BUILDPATH)/CLASSES:$(BUILDPATH)/PARSER:$(BUILDPATH)/LEXER:$(JCLASSLIB):$(JCLASSLIB)/JLex2.jar:$(JCLASSLIB)/yydebug.jar:$(JCLASSLIB)/asm-9.1.jar:

PARSER = javaparser
PARSER_CLASS = $(BUILDPATH)/PARSER/$(PARSER).class

SCANNER = scanner/scanner
LEXFILE = scanner/scanner
SCANNER_CLASS = $(BUILDPATH)/LEXER/$(SCANNER).class

DOTFILES = $(wildcard *.dot)
GRAPHS = $(addsuffix .jpg,$(DOTFILES))

USAGE:
	@echo "To create the executable run 'make build'"
	@echo "To run all tests run 'make tests'"
	@echo "To run the assembler tests run 'make asm_tests'"
	@echo "To run the typecheck tests run 'make typecheck_tests'"
	@echo "To run the grammar tests run 'make grammar_tests'"
	@echo "To run the scanner tests run 'make scanner_tests'"
	@echo "To clean the directory run 'make clean'"
	@echo "To show this help run 'make USAGE'"

### Tests ###

TEST_CLASS_PATH = $(CLASSPATH):$(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar

tests: asm_tests grammar_tests typecheck_tests scanner_tests

### Tests for self compiled code
COMPILE_TEST_DIR = tests/compile_tests
COMPILE_TEST_OUT_DIR =$(BUILDPATH)/tests/compile_tests
COMPILE_CLASS_PATH = $(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar:$(COMPILE_TEST_DIR)

compile_tests: tests/asm_tests/input_complex_examples/math_compare.class
	cp tests/asm_tests/input_complex_examples/math_compare.class tests/compile_tests/math.class
	
	$(JAVAC) -d $(COMPILE_TEST_OUT_DIR) -cp $(COMPILE_CLASS_PATH) $(COMPILE_TEST_DIR)/MathTests.java

	$(JAVA) -jar $(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar --disable-banner -cp $(COMPILE_TEST_DIR) -cp $(COMPILE_TEST_OUT_DIR) --scan-class-path


#### Asm Generation Tests
ASM_TEST_DIR = tests/asm_tests
ASM_TEST_OUT_DIR =$(BUILDPATH)/tests/asm_tests
ASM_TEST_SRCS = $(wildcard $(ASM_TEST_DIR)/*.java)
ASM_TEST_CLASSES = $(ASM_TEST_SRCS:$(ASM_TEST_DIR)/%.java=$(ASM_TEST_OUT_DIR)/%.class)

ASM_CLASS_PATH = $(TEST_CLASS_PATH):$(ASM_TEST_DIR)

asm_tests: $(ASM_TEST_CLASSES)
	$(JAVA) -jar $(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar --disable-banner -cp $(CLASSPATH) -cp $(ASM_TEST_OUT_DIR) --scan-class-path

# To run a specific test file
# asm_tests: $(ASM_TEST_OUT_DIR)/AssignTests.class
# 	$(JAVA) -jar $(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar --disable-banner -cp $(CLASSPATH) -cp $(ASM_TEST_OUT_DIR) --select-class AssignTests

# To run a specific test
# asm_tests: $(ASM_TEST_OUT_DIR)/ComplexTests.class
# 	$(JAVA) -jar $(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar --disable-banner -cp $(CLASSPATH) -cp $(ASM_TEST_OUT_DIR) --select-method ComplexTests#binary_tree

$(ASM_TEST_CLASSES): $(ASM_TEST_OUT_DIR)/%.class: $(ASM_TEST_DIR)/%.java $(SCANNER_CLASS) $(PARSER_CLASS)
	@echo "Build " $<
	$(JAVAC) -d $(ASM_TEST_OUT_DIR) -cp $(ASM_CLASS_PATH) $<

TYPECHECK_TEST_DIR = tests/typecheck_tests
TYPECHECK_TEST_OUT_DIR =$(BUILDPATH)/tests/typecheck_tests
TYPECHECK_TEST_SRCS = $(wildcard $(TYPECHECK_TEST_DIR)/*.java)
TYPECHECK_TEST_CLASSES = $(TYPECHECK_TEST_SRCS:$(TYPECHECK_TEST_DIR)/%.java=$(TYPECHECK_TEST_OUT_DIR)/%.class)

TYPECHECK_CLASS_PATH = $(TEST_CLASS_PATH):$(ASM_TEST_DIR)

typecheck_tests: $(TYPECHECK_TEST_CLASSES)
	$(JAVA) -jar $(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar --disable-banner -cp $(CLASSPATH) -cp $(TYPECHECK_TEST_OUT_DIR) --scan-class-path

$(TYPECHECK_TEST_CLASSES): $(TYPECHECK_TEST_OUT_DIR)/%.class: $(TYPECHECK_TEST_DIR)/%.java $(SCANNER_CLASS) $(PARSER_CLASS)
	@echo "Build " $<
	$(JAVAC) -d $(TYPECHECK_TEST_OUT_DIR) -cp $(TYPECHECK_CLASS_PATH) $<

GRAMMAR_TEST_DIR = tests/grammar_tests
GRAMMAR_TEST_OUT_DIR =$(BUILDPATH)/tests/grammar_tests
GRAMMAR_TEST_SRCS = $(wildcard $(GRAMMAR_TEST_DIR)/*.java)
GRAMMAR_TEST_CLASSES = $(GRAMMAR_TEST_SRCS:$(GRAMMAR_TEST_DIR)/%.java=$(GRAMMAR_TEST_OUT_DIR)/%.class)

GRAMMAR_CLASS_PATH = $(TEST_CLASS_PATH):$(ASM_TEST_DIR)

grammar_tests: $(GRAMMAR_TEST_CLASSES)
	$(JAVA) -jar $(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar --disable-banner -cp $(CLASSPATH) -cp $(GRAMMAR_TEST_OUT_DIR) --scan-class-path

$(GRAMMAR_TEST_CLASSES): $(GRAMMAR_TEST_OUT_DIR)/%.class: $(GRAMMAR_TEST_DIR)/%.java $(SCANNER_CLASS) $(PARSER_CLASS)
	@echo "Build " $<
	$(JAVAC) -d $(GRAMMAR_TEST_OUT_DIR) -cp $(GRAMMAR_CLASS_PATH) $<

SCANNER_TEST_DIR = tests/scanner_tests
SCANNER_TEST_OUT_DIR =$(BUILDPATH)/tests/scanner_tests
SCANNER_TEST_SRCS = $(wildcard $(SCANNER_TEST_DIR)/*.java)
SCANNER_TEST_CLASSES = $(SCANNER_TEST_SRCS:$(SCANNER_TEST_DIR)/%.java=$(SCANNER_TEST_OUT_DIR)/%.class)

SCANNER_CLASS_PATH = $(TEST_CLASS_PATH):$(ASM_TEST_DIR)

scanner_tests: $(SCANNER_TEST_CLASSES)
	$(JAVA) -jar $(JCLASSLIB)/junit-platform-console-standalone-1.8.0.jar --disable-banner -cp $(CLASSPATH) -cp $(SCANNER_TEST_OUT_DIR) --scan-class-path

$(SCANNER_TEST_CLASSES): $(SCANNER_TEST_OUT_DIR)/%.class: $(SCANNER_TEST_DIR)/%.java $(SCANNER_CLASS) $(PARSER_CLASS)
	@echo "Build " $<
	$(JAVAC) -d $(SCANNER_TEST_OUT_DIR) -cp $(SCANNER_CLASS_PATH) $<

### Debug helper ###

show_graphs: $(GRAPHS)
	$(foreach graph,$(GRAPHS),$(shell xdg-open $(graph)))

%.dot.jpg: %.dot
	dot -O -Tjpg $<

### Build ###
build: $(BUILDPATH)/MAIN/Main.class

$(BUILDPATH)/MAIN/Main.class: Main.java $(BUILDPATH)/LEXER/$(SCANNER).class $(BUILDPATH)/PARSER/$(PARSER).class
	$(JAVAC) -d $(BUILDPATH)/MAIN Main.java

$(BUILDPATH)/CLASSES/.sentinel: $(BUILDPATH)/CLASSES/DescriptorGenerator.class Classes.java
	$(JAVAC) -d $(BUILDPATH)/CLASSES Classes.java
	touch $(BUILDPATH)/CLASSES/.sentinel

# Build helper class DescriptorGenerator.java
$(BUILDPATH)/CLASSES/DescriptorGenerator.class: DescriptorGenerator.java
	$(JAVAC) -d $(BUILDPATH)/CLASSES DescriptorGenerator.java

$(PARSER_CLASS): $(PARSER).java
	$(JAVAC) -d $(BUILDPATH)/PARSER $(PARSER).java

yyTokenclass.class: $(PARSER).java
	$(JAVAC) -d $(BUILDPATH)/PARSER $(PARSER).java

$(SCANNER_CLASS): $(LEXFILE).java $(PARSER_CLASS)
	$(JAVAC) -d $(BUILDPATH)/LEXER $(LEXFILE).java

$(LEXFILE).java: $(LEXFILE)
	$(JAVA) JLex2.Main $(LEXFILE)

$(PARSER).java: $(PARSER).jay $(SKELETON) $(BUILDPATH)/CLASSES/.sentinel
	chmod u+x $(JAY) && $(JAY) $(JAOOY_FLAGS) $(PARSER).jay < $(SKELETON) > $(PARSER).java

remove_sentinels: $(BUILDPATH)/CLASSES/.sentinel
	rm $(BUILDPATH)/CLASSES/.sentinel

clean: remove_sentinels
	rm -f $(PARSER).java
	rm -f ${LEXFILE}.java
	rm -fr build/*
	rm $(BUILDPATH)/CLASSES/.sentinel
