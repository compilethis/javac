import java.util.Vector;
import java.util.stream.Collectors;
import java.util.HashMap;
import java.util.Map;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.objectweb.asm.*;

// Evtl statt String type : class TYPE mit String name
class CLASS {
	String name;
	Vector<MEMBER> members;

	public CLASS() {
		name = "null";
		members = new Vector<MEMBER>();
		if (System.getenv("DEBUG_CLASSES") != null) {
			this.createDOTFile();
		}
	}

	public CLASS(String name, Vector<MEMBER> members) {
		this.name = name;
		this.members = members;
		if (System.getenv("DEBUG_CLASSES") != null) {
			this.createDOTFile();
		}
	}

	public boolean containsMember(String name) {
		for (MEMBER member : members) {
			if (member.name.equals(name))
				return true;
		}
		return false;
	}

	public void createDOTFile() {
		try {
			FileWriter dotFileWriter = new FileWriter(this.name + ".dot");
			dotFileWriter.write(this.getDOTRepresentation());
			dotFileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// DOT Representation of the component and its children
	public String getDOTRepresentation() {

		String Representation = "";

		Representation += "\n//--------------------\n//Representation of " + name + ":\n" + "\ndigraph " + name + "{\n";

		for (MEMBER member : members) {
			Representation += "\n" + name + " -> " + "\"" + member.toString() + "\"";
			Representation += "\n" + member.getDOTRepresentation();
		}

		Representation += "\nsubgraph membersOf" + name + "{\n//rank=same\n";
		for (MEMBER member : members) {
			Representation += "\n" + "\"" + member.toString() + "\"";
		}

		Representation += "\n}// finish subgraph membersOf" + name;
		Representation += "\n}// finish digraph " + name;

		return Representation;

	}

	public void typeCheckAll() {
		System.out.println("\n--------------------\nMembers of " + name + ":");
		for (MEMBER member : members) {
			if (member instanceof METHOD) {
				System.out.println(member.name + "():" + member.typeCheck(new HashMap<String, String>(), this));
			} else {
				System.out.println(member.name + ":" + member.typeCheck(new HashMap<String, String>(), this));
			}
		}
		System.out.println("\nTypecheck on " + name + " executed.");
		System.out.println("--------------------\n");
	}

	public void typeCheck() {
		for (MEMBER member : members) {
			member.typeCheck(new HashMap<String, String>(), this);
		}
	}

	public byte[] codeGen(String sourceFileName) {
		ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
		cw.visit(Opcodes.V15, 0, name, null, "java/lang/Object", null);

		cw.visitSource(sourceFileName, null);

		Vector<FIELD> fields = members.stream().filter(member -> member instanceof FIELD).map(member -> (FIELD) member)
				.collect(Collectors.toCollection(Vector::new));

		// Create default constructor if class has no other constructor
		if (members.stream().allMatch(member -> !member.name.equals("<init>"))) {
			new METHOD("<init>", new Vector<PARAMETER>(), new BLOCK(new Vector<STMT>())).codeGen(cw, fields, this);
		}

		for (MEMBER member : members) {
			member.codeGen(cw, fields, this);
		}

		cw.visitEnd();

		return cw.toByteArray();
	}

}

abstract class MEMBER {
	String name;
	String type;

	public abstract String getDOTRepresentation();

	public abstract String typeCheck(Map<String, String> localVars, CLASS thisClass);

	public abstract void codeGen(ClassWriter cw, Vector<FIELD> fields, CLASS thisClass);
}

// added EXPRESSION for initializations
class FIELD extends MEMBER {
	EXPR expression;

	public FIELD(String type, String name) {
		this.type = type;
		this.name = name;
		this.expression = null;
	}

	public FIELD(String type, String name, EXPR expression) {
		this.type = type;
		this.name = name;
		this.expression = expression;
	}

	@Override
	public String getDOTRepresentation() {
		String Representation;
		Representation = "\"" + this.toString() + "\"" + "[ label=\"" + this.type + " " + this.name + "\" comment=\"";
		Representation += " type: " + this.type;
		Representation += " name: " + this.name;
		Representation += "\"]";

		if (this.expression != null) {
			Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + this.expression.toString() + "\"";
			Representation += "\n" + expression.getDOTRepresentation();
		}
		return Representation;
	}

	@Override
	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		if (name.equals(thisClass.name)){
			throw new Error(
					"Syntax error: Variable '" + name + "' is Class name. Declaring as a Variable is not allowed.");
		}
		
		if (expression == null) {
			return type;
		}
		String exprType = expression.typeCheck(localVars, thisClass);
		if (type.equals(exprType) || expression instanceof NULL) {
			return type;
		}
		throw new Error("Syntax Error: Field type does not match the expression type it was declared with. Field is '"
				+ type + "', but the expression is '" + exprType + "'.");


	}

	@Override
	public void codeGen(ClassWriter cw, Vector<FIELD> fields, CLASS thisClass) {
		cw.visitField​(0, this.name, DescriptorGenerator.getTypeDescriptor(this.type), null, null);
	}
}

// Changed STMT to Vector of STMT
class METHOD extends MEMBER {
	Vector<PARAMETER> parameters;
	BLOCK statement;

	public METHOD(String type, String name, Vector<PARAMETER> parameters, BLOCK statement) {
		this.type = type;
		this.name = name;
		this.parameters = parameters;
		this.statement = statement;
	}

	// for constructors
	public METHOD(String name, Vector<PARAMETER> parameters, BLOCK statement) {
		this.type = "void";
		this.name = "<init>";
		this.parameters = parameters;
		this.statement = statement;
	}

	public String getDOTRepresentation() {
		String Representation;
		Representation = "\"" + this.toString() + "\"" + " [ label=\"" + this.type + " " + this.name
				+ "()\", comment=\"";
		Representation += "type: " + this.type + "";
		Representation += "name: " + this.name + "";
		Representation += "\"]\n";
		Representation += "\"" + this.toString() + "\"" + " -> " + "\"" + this.statement.toString() + "\"";
		Representation += "\n" + this.statement.getDOTRepresentation();
		for (PARAMETER parameter : parameters) {
			Representation += "\n- " + parameter.getDOTRepresentation();
		}
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		for (PARAMETER parameter : parameters) {
			localVars.put(parameter.name, parameter.typeCheck(localVars, thisClass));
		}
		// int a
		String stmtType = statement.typeCheck(localVars, thisClass);
		if (type.equals(stmtType)) {
			return type;
		}
		// throw exception
		throw new Error("Syntax error: Method type is '" + type + "' but the block returns '" + stmtType + "'. Method: "
				+ name);
	}

	@Override
	public void codeGen(ClassWriter cw, Vector<FIELD> fields, CLASS thisClass) {
		HashMap<String, Integer> stackMap = new HashMap<>();
		stackMap.put("this", stackMap.size());

		String typeDescriptor = "(";
		for (PARAMETER parameter : parameters) {
			typeDescriptor += DescriptorGenerator.getTypeDescriptor(parameter.type);

			// TO DO: Always +1?
			// https://asm.ow2.io/javadoc/org/objectweb/asm/Type.html#getSize()
			stackMap.put(parameter.name, stackMap.size());
		}
		typeDescriptor += ")";
		typeDescriptor += DescriptorGenerator.getTypeDescriptor(this.type);

		MethodVisitor method = cw.visitMethod(0, this.name, typeDescriptor, null, null);
		method.visitCode();

		// Super call for constructors
		if (this.name.equals("<init>")) {
			method.visitVarInsn(Opcodes.ALOAD, 0);
			method.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
			fields.forEach(field -> {
				if (field.expression != null) {
					ASSIGN assign = new ASSIGN(new THIS(), field.name, field.expression);
					assign.type = field.type;
					assign.codeGen(method, stackMap, thisClass);
				}
			});
		}

		statement.codeGen(method, stackMap, thisClass);

		if (!statement.hasReturn()) {
			method.visitInsn(Opcodes.RETURN);
		}

		method.visitMaxs(0, 0); // As we use COMPUTE_MAXS, we can call this method with any param
		method.visitEnd();
	}
}

/*
 * class CONSTRUCTOR extends MEMBER { Vector<PARAMETER> parameters; BLOCK
 * statement;
 *
 * class CONSTRUCTOR extends MEMBER { Vector<PARAMETER> parameters; BLOCK
 * statement;
 *
 * public CONSTRUCTOR(String type,Vector<PARAMETER> parameters, BLOCK statement)
 * { this.type = type; this.name = type; this.parameters = parameters;
 * this.statement = statement; }
 *
 * public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
 * if(statement.typeCheck(localVars, thisClass).)
 *
 * return thisClass.name; }
 */

class PARAMETER {
	String type;
	String name;

	public PARAMETER(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getDOTRepresentation() {
		return "\"" + this.toString() + "\"" + "[comment=\"type: " + this.type + " name: " + this.name + "\"]";
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		return type;
	}
}

abstract class STMT {
	public abstract String getDOTRepresentation();

	public abstract String typeCheck(Map<String, String> localVars, CLASS thisClass);

	public abstract void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass);
}

class BLOCK extends STMT {
	Vector<STMT> statements;

	public BLOCK(Vector<STMT> statements) {
		this.statements = statements;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		Map<String, String> localVarsCopy = new HashMap<>(localVars);

		Vector<String> returns = new Vector<String>();
		for (STMT statement : statements) {
			if (statement instanceof LOCAL_VAR) {
				LOCAL_VAR lovar = (LOCAL_VAR) statement;
				localVarsCopy.put(lovar.name, lovar.typeCheck(localVarsCopy, thisClass));
			} else if (!statement.typeCheck(localVarsCopy, thisClass).equals("void")) {
				if (statement instanceof BLOCK || statement instanceof WHILE || statement instanceof IF
						|| statement instanceof RETURN) {
					returns.add(statement.typeCheck(localVarsCopy, thisClass));
				}

			}
		}
		if (returns.size() == 0) {
			return "void";
		}
		if (returns.stream().distinct().count() == 1) {
			return returns.get(0);
		} else {
			throw new Error("Syntax error: A block has multiple returns, but their types do not match.");
		}
	}

	@Override
	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\n" + "\"" + this.toString() + "\"" + " [ label=\"Block\" ]";
		for (STMT statement : statements) {
			Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + statement.toString() + "\"";
			Representation += "\n" + statement.getDOTRepresentation();
		}
		return Representation;
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		HashMap<String, Integer> stackMap = new HashMap<>(stack);

		for (STMT statement : statements) {
			statement.codeGen(method, stackMap, thisClass);

			// Method calls where the return type is thrown away need special handling
			if (statement instanceof STMT_EXPR_STMT) {
				STMT_EXPR_STMT stmt_expr_stmt = (STMT_EXPR_STMT) statement;
				if (stmt_expr_stmt.statement_expression instanceof METHOD_CALL) {
					METHOD_CALL method_call = (METHOD_CALL) stmt_expr_stmt.statement_expression;

					if (!method_call.type.equals("void"))
						method.visitInsn​(Opcodes.POP);
				}
			}
		}
	}

	public boolean hasReturn() {
		return statements.stream().anyMatch(stmt -> stmt instanceof RETURN);
	}
}

class RETURN extends STMT {
	EXPR expression;

	public RETURN() {
		this.expression = null;
	}

	public RETURN(EXPR expression) {
		this.expression = expression;
	}

	@Override
	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\"" + this.toString() + "\"" + " -> " + "\"" + this.expression.toString() + "\"";
		Representation += "\n" + this.expression.getDOTRepresentation();
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		if (expression == null) {
			return "void";
		} else {
			return expression.typeCheck(localVars, thisClass);
		}
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		if (expression != null) {
			expression.codeGen(method, stack, thisClass);

			int opcode = DescriptorGenerator.getOpcode(expression.type, Opcodes.IRETURN);
			method.visitInsn(opcode);
		} else {
			method.visitInsn(Opcodes.RETURN);
		}
	}
}

class WHILE extends STMT {
	EXPR expression;
	STMT statement;

	public WHILE(EXPR expression, STMT statement) {
		this.expression = expression;
		this.statement = statement;
	}

	@Override
	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\"" + this.toString() + "\"" + " -> " + "\"" + this.expression.toString() + "\"";
		Representation += "\"" + this.toString() + "\"" + " -> " + "\"" + this.statement.toString() + "\"";
		Representation += "\n" + this.expression.getDOTRepresentation();
		Representation += "\n" + this.statement.getDOTRepresentation();
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		String bExpression = expression.typeCheck(localVars, thisClass);
		String typeStmt1 = statement.typeCheck(localVars, thisClass);
		if (bExpression.equals("boolean")) {
			return typeStmt1;
		}
		throw new Error("Syntax error: While-Condition must be of type 'boolean'. Given was '" + bExpression + "'.");
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		Label loop = new Label();
		Label end = new Label();

		method.visitLabel(loop);

		expression.codeGen(method, stack, thisClass, end);

		statement.codeGen(method, stack, thisClass);

		method.visitJumpInsn(Opcodes.GOTO, loop);

		method.visitLabel(end);
	}
}

class LOCAL_VAR extends STMT {
	String type;
	String name;
	EXPR expression;

	// Beispiel: int a;
	public LOCAL_VAR(String type, String name) {
		this(type, name, null);
	}

	// Beispiel: int a = 3;
	public LOCAL_VAR(String type, ASSIGN assign) {
		this.type = type;
		this.name = assign.name;
		this.expression = assign.expression;
	}

	// Beispiel: int a = 3;
	public LOCAL_VAR(String type, String name, EXPR expression) {
		this.type = type;
		this.name = name;
		this.expression = expression;
	}

	@Override
	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\"" + this.toString() + "\"" + " [ label=\"" + this.type + " " + this.name
				+ "\", comment=\"name: " + this.name + " type: " + this.type + "\"]";
		Representation += "\"" + this.toString() + "\"" + " -> " + "\"" + this.expression.toString() + "\"";
		Representation += "\n" + this.expression.getDOTRepresentation();
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		if (localVars.containsKey(name) || thisClass.containsMember(name)) {
			throw new Error(
					"Syntax error: Variable '" + name + "' was declared more than once. Redeclaring is not allowed.");
		}
		if (name.equals(thisClass.name)){
			throw new Error(
					"Syntax error: Variable '" + name + "' is Class name. Declaring as a Variable is not allowed.");
		}
		if (expression == null) {
			return type;
		}
		String exprType = expression.typeCheck(localVars, thisClass);
		if (type.equals(exprType) || expression instanceof NULL) {
			return exprType;
		}

		throw new Error("Syntax error: Given expression type '" + exprType + "' does not match '" + type + "'.");
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {

		if (expression != null) {
			expression.codeGen(method, stack, thisClass);

			int opcode = DescriptorGenerator.getOpcode(this.type, Opcodes.ISTORE);
			method.visitVarInsn(opcode, stack.size());
		}

		// TO DO: Always +1?
		// https://asm.ow2.io/javadoc/org/objectweb/asm/Type.html#getSize()
		stack.put(this.name, stack.size());
	}
}

// Evtl nochmal EXPR und STMT auslagern in eine Klasse
// Evtl Vektoren --> mehrere ELSE IF
class IF extends STMT {
	EXPR expression;
	STMT if_statement;
	STMT else_statement;

	public IF(EXPR expression, STMT if_statement) {
		this.expression = expression;
		this.if_statement = if_statement;
		this.else_statement = null;
	}

	public IF(EXPR expression, STMT if_statement, STMT else_statement) {
		this.expression = expression;
		this.if_statement = if_statement;
		this.else_statement = else_statement;
	}

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\"" + this.toString() + "\"" + " -> " + "\"" + this.expression.toString() + "\"";
		Representation += this.expression.getDOTRepresentation();
		Representation += "\"" + this.toString() + "\"" + " -> " + "\"" + this.if_statement.toString() + "\"";
		Representation += this.if_statement.getDOTRepresentation();
		if (this.else_statement != null) {
			Representation += "\"" + this.toString() + "\"" + " -> " + "\"" + this.else_statement.toString() + "\"";
			Representation += this.else_statement.getDOTRepresentation();
		}
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		String ifStmtType = if_statement.typeCheck(localVars, thisClass);

		String bExpression = expression.typeCheck(localVars, thisClass);
		if (bExpression.equals("boolean")) {
			if (else_statement == null) {
				return ifStmtType;
			}

			String elseStmtType = else_statement.typeCheck(localVars, thisClass);

			if (ifStmtType.equals(elseStmtType)) {
				return ifStmtType;
			} else {
				throw new Error("Syntax error: If-Block and else-Block do not return the same type. IF:'" + ifStmtType
						+ "' ELSE:'" + elseStmtType + "'.");
			}
		}
		throw new Error("Syntax error: While-Condition must be of type boolean. Given was '" + bExpression + "'.");
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		Label end = new Label();
		Label else_label = new Label();

		expression.codeGen(method, stack, thisClass, else_statement == null ? end : else_label);

		if_statement.codeGen(method, stack, thisClass);

		if (else_statement != null) {
			method.visitJumpInsn(Opcodes.GOTO, end);
			method.visitLabel(else_label);
			else_statement.codeGen(method, stack, thisClass);
		}

		method.visitLabel(end);
	}
}

class STMT_EXPR_STMT extends STMT {
	STMT_EXPR statement_expression;

	public STMT_EXPR_STMT(STMT_EXPR statement_expression) {
		this.statement_expression = statement_expression;
	}

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + this.statement_expression.toString()
				+ "\"";
		Representation += "\n" + "\"" + this.toString() + "\" [ label=\"PROXY\"]";
		Representation += "\n" + this.statement_expression.getDOTRepresentation();
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		return statement_expression.typeCheck(localVars, thisClass);
	}

	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		statement_expression.codeGen(method, stack, thisClass);
	}
}

abstract class STMT_EXPR {
	String type;

	public abstract String getDOTRepresentation();

	public abstract String typeCheck(Map<String, String> localVars, CLASS thisClass);

	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
	}
}

class ASSIGN extends STMT_EXPR {
	EXPR beforeNameExpression;
	String name;
	EXPR expression;

	public ASSIGN(String name, EXPR expression) {
		this(null, name, expression);
	}

	public ASSIGN(EXPR beforeNameExpression, String name, EXPR expression) {
		this.beforeNameExpression = beforeNameExpression;
		this.name = name;
		this.expression = expression;
	}

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\n" + "\"" + this.toString() + "\"" + "[ comment=\"name: " + this.name + "\"]";
		Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + this.expression.toString() + "\"";
		Representation += "\n" + this.expression.getDOTRepresentation();
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		String exprType = expression.typeCheck(localVars, thisClass);
		String beforenametype = "";
		if (this.beforeNameExpression != null) {
			beforenametype = this.beforeNameExpression.typeCheck(localVars, thisClass);
			if (beforenametype.equals(thisClass.name)) {
				for (MEMBER mem : thisClass.members) {
					if (mem.name.equals(name) && mem instanceof FIELD) {
						if (mem.typeCheck(localVars, thisClass).equals(exprType) || expression instanceof NULL) {
							type = exprType;
							return type;
						}
					}
				}
			} else {
				throw new Error("Syntax error: Only this. and objectsfrom type: " + thisClass.name + " are allowed.");
			}
		}

		if (localVars.containsKey(name)) {
			if (localVars.get(name).equals(exprType) || expression instanceof NULL) {
				type = exprType;
				return type;
			}
		} else {
			for (MEMBER mem : thisClass.members) {
				if (mem.name.equals(name) && mem instanceof FIELD) {
					if (mem.typeCheck(localVars, thisClass).equals(exprType) || expression instanceof NULL) {
						if (this.beforeNameExpression == null)
							this.beforeNameExpression = new THIS();
						type = exprType;
						return type;
					}
				}
			}
		}
		// throw exception
		throw new Error("Syntax error: Expression unknown.");
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		if (this.beforeNameExpression == null) {
			// local variable
			this.expression.codeGen(method, stack, thisClass);
			int opcode = DescriptorGenerator.getOpcode(this.type, Opcodes.ISTORE);
			method.visitVarInsn(opcode, stack.get(this.name));
		} else {
			// field variable
			this.beforeNameExpression.codeGen(method, stack, thisClass);
			this.expression.codeGen(method, stack, thisClass);
			method.visitFieldInsn(Opcodes.PUTFIELD, thisClass.name, this.name,
					DescriptorGenerator.getTypeDescriptor(this.type));
		}
	}
}

class NEW extends STMT_EXPR {
	Vector<EXPR> parameters;

	public NEW(String type, Vector<EXPR> parameters) {
		this.type = type;
		this.parameters = parameters;
	}

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\n" + "\"" + this.toString() + "\"" + "[ comment=\" type: " + this.type + "\"]";
		for (EXPR parameter : parameters) {
			Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + parameter.toString() + "\"";
			Representation += "\n" + parameter.getDOTRepresentation();
		}
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		return type;
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		method.visitTypeInsn​(Opcodes.NEW, DescriptorGenerator.getTypeName(this.type));
		method.visitInsn​(Opcodes.DUP);

		String typeDescriptor = "(";
		for (EXPR param : parameters) {
			param.codeGen(method, stack, thisClass);
			typeDescriptor += DescriptorGenerator.getTypeDescriptor(param.type);
		}
		typeDescriptor += ")";
		typeDescriptor += DescriptorGenerator.getTypeDescriptor("void");

		method.visitMethodInsn​(Opcodes.INVOKESPECIAL, DescriptorGenerator.getTypeName(this.type), "<init>",
				typeDescriptor);
	}
}

class METHOD_CALL extends STMT_EXPR {
	EXPR expression;
	String method_name;
	Vector<EXPR> parameters;

	public METHOD_CALL(String method_name, Vector<EXPR> parameters) {
		this.expression = new THIS();
		this.method_name = method_name;
		this.parameters = parameters;
	}

	public METHOD_CALL(EXPR expression, String method_name, Vector<EXPR> parameters) {
		this.expression = expression;
		this.method_name = method_name;
		this.parameters = parameters;
	}

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\"" + this.toString() + "\"" + " [label=\"call " + this.method_name
				+ "\", comment=\" method_name: " + this.method_name + "\"]";
		Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + this.expression.toString() + "\"";
		Representation += "\n" + this.expression.getDOTRepresentation();
		for (EXPR parameter : parameters) {
			Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + parameter.toString() + "\"";
			Representation += "\n" + parameter.getDOTRepresentation();
		}
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		String exprType = expression.typeCheck(localVars, thisClass);
		if (exprType.equals(thisClass.name)) {
			for (MEMBER mem : thisClass.members) {
				if (mem.name.equals(method_name)) {
					METHOD met = (METHOD) mem;
					if (parameters.size() == met.parameters.size()) {
						for (int i = 0; i < met.parameters.size(); i++) {
							if (!parameters.get(i).typeCheck(localVars, thisClass).equals(met.parameters.get(i).type)) {
								throw new Error(
										"Syntax error: Parameters are not of the correct type. Method: " + method_name);
							}
						}
						type = met.type;
						return type;
					} else {
						throw new Error("Syntax error: " + method_name + "() needs " + met.parameters.size()
								+ " parameters, but " + parameters.size() + " were given.");
					}
				}
			}
		}
		throw new Error("Syntax error: Unknown Method '" + method_name + "'.");
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		this.expression.codeGen(method, stack, thisClass);

		String typeDescriptor = "(";
		for (EXPR param : parameters) {
			param.codeGen(method, stack, thisClass);
			typeDescriptor += DescriptorGenerator.getTypeDescriptor(param.type);
		}
		typeDescriptor += ")";
		typeDescriptor += DescriptorGenerator.getTypeDescriptor(this.type);

		method.visitMethodInsn(Opcodes.INVOKEVIRTUAL, thisClass.name, this.method_name, typeDescriptor);
	}
}

abstract class EXPR {
	String type;

	public abstract String getDOTRepresentation();

	abstract public String typeCheck(Map<String, String> localVars, CLASS thisClass);

	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		System.out.println("Unknown expression");
	}

	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass, Label jumpLabel) {

	}
}

class THIS extends EXPR {

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\"" + this.toString() + "\"" + "[ comment=\"type : this\" ]";
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		type = thisClass.name;
		return type;
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		method.visitVarInsn(Opcodes.ALOAD, 0);
	}
}

class SUPER extends EXPR {

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\"" + this.toString() + "\"" + "[ comment=\"type : super\" ]";
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		type = thisClass.name;
		return type;
	}
}

class LOCAL_OR_FIELD_VAR extends EXPR {
	String name;

	public LOCAL_OR_FIELD_VAR(String name) {
		this.name = name;
	}

	public String toString() {
		// See
		// https://stackoverflow.com/questions/36376339/how-does-object-tostring-get-the-memory-address-and-how-can-i-mimic-it
		return getClass().getName() + '_' + this.name + '@' + Integer.toHexString(hashCode());
	}

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\"" + this.toString() + "\"" + "[ label=\"" + this.name + "\", comment=\"name : " + this.name
				+ "\" ]";
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		if (localVars.containsKey(name)) {
			type = localVars.get(name);
			return type;
		}
		for (MEMBER mem : thisClass.members) {
			if (mem.name.equals(name)) {
				type = mem.typeCheck(localVars, thisClass);
				return type;
			}
		}
		throw new Error("Syntax error: Unknown Variable '" + name + "'.");
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		if (stack.containsKey(this.name)) {
			// local variable
			int opcode = DescriptorGenerator.getOpcode(this.type, Opcodes.ILOAD);
			method.visitVarInsn(opcode, stack.get(this.name));
		} else {
			// field variable
			method.visitVarInsn(Opcodes.ALOAD, 0);
			method.visitFieldInsn(Opcodes.GETFIELD, thisClass.name, this.name,
					DescriptorGenerator.getTypeDescriptor(this.type));
		}
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass, Label jumpLabel) {
		codeGen(method, stack, thisClass);
		method.visitJumpInsn(Opcodes.IFEQ, jumpLabel);
	}
}

class INST_VAR extends EXPR {
	EXPR expression;
	String name;

	// z. B.: a.x oder haus.zimmer

	public INST_VAR(EXPR expression, String name) {
		this.expression = expression;
		this.name = name;
	}

	public String toString() {
		// See
		// https://stackoverflow.com/questions/36376339/how-does-object-tostring-get-the-memory-address-and-how-can-i-mimic-it
		return getClass().getName() + '_' + this.expression.toString() + "_DOT_" + this.name + '@'
				+ Integer.toHexString(hashCode());
	}

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\n" + "\"" + this.toString() + "\"" + "[ label=\"" + this.name + "\", comment=\"name: "
				+ this.name + "\"]";
		Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + this.expression.toString() + "\"";
		Representation += "\n" + this.expression.getDOTRepresentation();
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		if (expression.typeCheck(localVars, thisClass).equals(thisClass.name)) {
			for (MEMBER mem : thisClass.members) {
				if (mem.name.equals(name)) {
					type = mem.typeCheck(localVars, thisClass);
					return type;
				}
			}
		}
		throw new Error("Syntax error: Unknown Variable '" + name + "'.");
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		this.expression.codeGen(method, stack, thisClass);
		method.visitFieldInsn(Opcodes.GETFIELD, thisClass.name, this.name,
				DescriptorGenerator.getTypeDescriptor(this.type));
	}
}

class UNARY extends EXPR {
	String operator;
	EXPR expression;

	public UNARY(String operator, EXPR expression) {
		this.operator = operator;
		this.expression = expression;
	}

	public String getDOTRepresentation() {
		String Representation = "";
		Representation += "\n" + "\"" + this.toString() + "\"" + "[comment=\"operator: " + this.operator + "\"]";
		Representation += "\n" + "\"" + this.toString() + "\"" + "\"" + this.expression.toString() + "\"";
		Representation += "\n" + this.expression.getDOTRepresentation();
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		type = expression.typeCheck(localVars, thisClass);
		if (type.equals("int")) {
			if (operator.equals("!")) {
				throw new Error("Syntax Error: '!' can not be applied to 'int'.");
			}
			return type;
		} else if (type.equals("char")) {
			if (operator.equals("!")) {
				throw new Error("Syntax Error: '!' can not be applied to 'int'.");
			}
			type = "int"; // Intentional cast to int
			return type;
		} else if (type.equals("boolean")) {
			if (!operator.equals("!")) {
				throw new Error("Syntax Error: " + operator + " can not be applied to 'int'.");
			}
			return type;
		} else {
			throw new Error("Syntax Error: Unaries cannot be applied to '" + type + "'.");
		}
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		// Handle negative constants like int i = -42;
		if (expression instanceof INTEGER && operator.equals("-")) {
			INTEGER intExpression = (INTEGER) expression;
			intExpression.zahl = -intExpression.zahl;
			expression = intExpression;
			operator = "";
		}

		expression.codeGen(method, stack, thisClass);

		switch (operator) {
			case "+":
				break;
			case "-":
				int opcode = DescriptorGenerator.getOpcode(this.type, Opcodes.INEG);
				method.visitInsn(opcode);
				break;
			case "!":
				codeGenBooleanOperand(method);
				break;
		}
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass, Label jumpLabel) {
		expression.codeGen(method, stack, thisClass);

		// Operator always has to be "!" in that case
		method.visitJumpInsn(Opcodes.IFNE, jumpLabel);
	}

	private void codeGenBooleanOperand(MethodVisitor method) {
		Label otherCase = new Label();
		Label end = new Label();

		method.visitJumpInsn(Opcodes.IFNE, otherCase);

		method.visitInsn(Opcodes.ICONST_1);
		method.visitJumpInsn(Opcodes.GOTO, end);

		method.visitLabel(otherCase);
		method.visitInsn(Opcodes.ICONST_0);

		method.visitLabel(end);

	}
}

class BINARY extends EXPR {
	EXPR exp_1;
	String operator;
	EXPR exp_2;

	public BINARY(EXPR exp_1, String operator, EXPR exp_2) {
		this.exp_1 = exp_1;
		this.operator = operator;
		this.exp_2 = exp_2;
	}

	public String toString() {
		// See
		// https://stackoverflow.com/questions/36376339/how-does-object-tostring-get-the-memory-address-and-how-can-i-mimic-it
		return getClass().getName() + '_' + '(' + '_' + this.exp_1.toString() + '_' + this.operator + '_'
				+ this.exp_2.toString() + '_' + '_' + ')' + '@' + Integer.toHexString(hashCode());
	}

	public String getDOTRepresentation() {
		String Representation;
		Representation = "\"" + this.toString() + "\"" + " [ label=\"" + this.operator + "\", comment=\"";
		Representation += " exp_1: " + this.exp_1;
		Representation += " exp_2: " + this.exp_2;
		Representation += " operator: " + this.operator;
		Representation += "\"]";
		Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + this.exp_1.toString() + "\"";
		Representation += "\n" + this.exp_1.getDOTRepresentation();
		Representation += "\n" + "\"" + this.toString() + "\"" + " -> " + "\"" + this.exp_2.toString() + "\"";
		Representation += "\n" + this.exp_2.getDOTRepresentation();
		return Representation;
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		String typeExpr1 = exp_1.typeCheck(localVars, thisClass);
		String typeExpr2 = exp_2.typeCheck(localVars, thisClass);
		if (typeExpr1.equals(typeExpr2)) {
			this.type = operator.matches("<|<=|==|!=|>|>=") ? "boolean" : typeExpr1;
			// We only check of the expressions since they are equal in this case
			if (!operator.matches("==|!=") && typeExpr1.equals("null")) {
				throw new Error("Syntax Error: Using null in binary expressions is only legal with '==' or '!='.");
			}
			if (typeExpr1.equals("String") && operator.matches("\\+|\\-|\\/|\\*|<|<=|>|>=")) {
				// Note: We do not allow '+' for Strings intentionally, since String
				// concatination is a complicated internal Method
				throw new Error("Syntax Error: " + "operator" + " can not be applied to Strings.");
			}
			if (typeExpr1.equals(thisClass.name) && operator.matches("\\+|\\-|\\/|\\*|<|<=|>|>=")) {
				throw new Error("Syntax Error: " + "operator" + " can not be applied to " + thisClass.name + ".");
			}
			return this.type;
		} else if (operator.matches("==|!=") && (typeExpr1.equals("null") || typeExpr2.equals("null"))) {
			if (typeExpr2.matches("String|" + thisClass.name) || typeExpr1.matches("String|" + thisClass.name)) {
				this.type = "boolean";
				return this.type;
			}
		}
		throw new Error("Syntax error: '" + typeExpr1 + "' is not compatible with " + typeExpr2 + ".");
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		exp_1.codeGen(method, stack, thisClass);
		exp_2.codeGen(method, stack, thisClass);

		switch (operator) {
			case "+":
				codeGenOperand(method, Opcodes.IADD);
				break;
			case "-":
				codeGenOperand(method, Opcodes.ISUB);
				break;
			case "*":
				codeGenOperand(method, Opcodes.IMUL);
				break;
			case "/":
				codeGenOperand(method, Opcodes.IDIV);
				break;
			case "&":
				codeGenOperand(method, Opcodes.IAND);
				break;
			case "|":
				codeGenOperand(method, Opcodes.IOR);
				break;
			case "^":
				codeGenOperand(method, Opcodes.IXOR);
				break;
			case "<<":
				codeGenOperand(method, Opcodes.ISHL);
				break;
			case ">>":
				codeGenOperand(method, Opcodes.ISHR);
				break;
			case ">>>":
				codeGenOperand(method, Opcodes.IUSHR);
				break;
			case "<":
			case "<=":
			case "==":
			case "!=":
			case ">=":
			case ">":
				codeGenBooleanOperand(method);
				break;
		}
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass, Label jumpLabel) {
		exp_1.codeGen(method, stack, thisClass);
		exp_2.codeGen(method, stack, thisClass);

		codeGenJump(method, jumpLabel);
	}

	private void codeGenBooleanOperand(MethodVisitor method) {
		Label otherCase = new Label();
		Label end = new Label();
		codeGenJump(method, otherCase);

		method.visitInsn(Opcodes.ICONST_1);
		method.visitJumpInsn(Opcodes.GOTO, end);

		method.visitLabel(otherCase);
		method.visitInsn(Opcodes.ICONST_0);

		method.visitLabel(end);

	}

	private void codeGenOperand(MethodVisitor method, int opcode) {
		int genericOpcode = DescriptorGenerator.getOpcode(exp_1.type, opcode);
		method.visitInsn(genericOpcode);
	}

	private void codeGenJump(MethodVisitor method, Label jumpLabel) {
		// Note that the opcode is exactly the other way round as we then jump (i.e if
		// evaluates to false)
		switch (operator) {
			case "<":
				method.visitJumpInsn(Opcodes.IF_ICMPGE, jumpLabel);
				break;
			case "<=":
				method.visitJumpInsn(Opcodes.IF_ICMPGT, jumpLabel);
				break;
			case "==":
				method.visitJumpInsn(Opcodes.IF_ICMPNE, jumpLabel);
				break;
			case "!=":
				method.visitJumpInsn(Opcodes.IF_ICMPEQ, jumpLabel);
				break;
			case ">=":
				method.visitJumpInsn(Opcodes.IF_ICMPLT, jumpLabel);
				break;
			case ">":
				method.visitJumpInsn(Opcodes.IF_ICMPLE, jumpLabel);
				break;
		}
	}
}

class INTEGER extends EXPR {
	int zahl;

	public INTEGER(String zahl) {
		this.zahl = Integer.parseInt(zahl);
		this.type = "int";
	}

	public String getDOTRepresentation() {
		return "\"" + this.toString() + "\"" + "[ label=\"" + this.zahl + "\", comment=\"" + this.zahl + "\"]";
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		return type;
	}

	public String toString() {
		// See
		// https://stackoverflow.com/questions/36376339/how-does-object-tostring-get-the-memory-address-and-how-can-i-mimic-it
		return "int_" + this.zahl + '@' + Integer.toHexString(hashCode());
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		if (zahl >= -1 && zahl <= 5)
			method.visitInsn(Opcodes.ICONST_0 + zahl);
		else if (zahl >= Byte.MIN_VALUE && zahl <= Byte.MAX_VALUE)
			method.visitIntInsn​(Opcodes.BIPUSH, zahl);
		else if (zahl >= Short.MIN_VALUE && zahl <= Short.MAX_VALUE)
			method.visitIntInsn​(Opcodes.SIPUSH, zahl);
		else
			method.visitLdcInsn(zahl);
	}
}

class BOOL extends EXPR {
	boolean bool;

	public BOOL(String bool) {
		this.bool = Boolean.parseBoolean(bool);
		this.type = "boolean";
	}

	public String getDOTRepresentation() {
		return "\"" + this.toString() + "\"" + "[ comment=\"" + this.bool + "\"]";
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		return type;
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		if (bool)
			method.visitInsn(Opcodes.ICONST_1);
		else
			method.visitInsn(Opcodes.ICONST_0);
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass, Label jumpLabel) {
		codeGen(method, stack, thisClass);
		method.visitJumpInsn(Opcodes.IFEQ, jumpLabel);
	}
}

class CHAR extends EXPR {
	char zeichen;

	public CHAR(String zeichen) {
		this.zeichen = zeichen.charAt(1);
		this.type = "char";
	}

	public String getDOTRepresentation() {
		return "\"" + this.toString() + "\"" + "[ label=\"" + this.zeichen + "\"]";
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		return type;
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		method.visitIntInsn​(Opcodes.BIPUSH, zeichen);
	}

}

class STRING extends EXPR {
	String text;

	public STRING(String text) {
		this.text = text.replaceAll("\"", "");
		this.type = "String";
	}

	public String getDOTRepresentation() {
		// TODO Proper escaping
		return "\"" + this.toString() + "\"" + "[ label=\"" + this.text + "\"]";
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		return type;
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		method.visitLdcInsn(text);
	}
}

class NULL extends EXPR {
	public NULL() {
		this.type = "null";
	}

	public String getDOTRepresentation() {
		return "\"" + this.toString() + "\"" + "[ comment=\"NULL\"]";
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		return type;
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		method.visitInsn(Opcodes.ACONST_NULL);
	}
}

class STMT_EXPR_EXPR extends EXPR {
	STMT_EXPR statement_expression;

	public STMT_EXPR_EXPR(STMT_EXPR statement_expression) {
		this.statement_expression = statement_expression;
	}

	public String getDOTRepresentation() {
		return "\"" + this.toString() + "\"" + " -> " + "\"" + this.statement_expression.toString() + "\"";
	}

	public String typeCheck(Map<String, String> localVars, CLASS thisClass) {
		type = statement_expression.typeCheck(localVars, thisClass);
		return type;
	}

	@Override
	public void codeGen(MethodVisitor method, Map<String, Integer> stack, CLASS thisClass) {
		statement_expression.codeGen(method, stack, thisClass);
	}
}
