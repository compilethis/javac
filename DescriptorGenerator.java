import org.objectweb.asm.*;

class DescriptorGenerator {
    public static String getTypeDescriptor(String type) {
        switch (type) {
            case "int":
                return Type.getDescriptor(int.class);
            case "void":
                return Type.getDescriptor(void.class);
            case "String":
                return Type.getDescriptor(String.class);
            case "boolean":
                return Type.getDescriptor(boolean.class);
            case "char":
                return Type.getDescriptor(char.class);
            default:
                return "L" + type + ";";
        }
    }

    public static String getTypeName(String type) {
        switch (type) {
            case "int":
                return Type.getInternalName​(int.class);
            case "void":
                return Type.getInternalName​(void.class);
            case "String":
                return Type.getInternalName​(String.class);
            case "boolean":
                return Type.getInternalName​(boolean.class);
            case "char":
                return Type.getInternalName​(char.class);
            default:
                return type;
        }
    }

    public static int getOpcode(String type, int opcode) {
        return Type.getType​(DescriptorGenerator.getTypeDescriptor(type)).getOpcode(opcode);
    }
}