import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.objectweb.asm.*;

import static org.junit.jupiter.api.Assertions.fail;

public class AsmTestHelper {

    private JavaCompiler javac;
    private javaparser parser;

    private String input_folder;

    AsmTestHelper(String input_folder) {
        this.javac = ToolProvider.getSystemJavaCompiler();
        this.parser = new javaparser();
        this.input_folder = input_folder;
    }

    public scanner createScanner(String filename) throws FileNotFoundException {
        String filePath = new File("").getAbsolutePath();

        InputStream inputStream = new FileInputStream(filePath.concat(input_folder + filename + ".java"));
        return new scanner(new java.io.InputStreamReader(inputStream));
    }

    public String getInputFilePath(String filename) throws FileNotFoundException {
        String filePath = new File("").getAbsolutePath();

        return filePath.concat(input_folder + filename);
    }

    public void compileReference(String fileName) throws FileNotFoundException {
        javac.run(null, null, null, getInputFilePath(fileName));
    }

    public void compile(scanner scanner, String sourceFileName) {
        try {
            Object result = parser.yyparse(scanner);
            if (result instanceof CLASS) {
                CLASS resultClass = (CLASS) result;
                resultClass.typeCheck();
                writeClassfile(resultClass.codeGen(sourceFileName));
            } else {
                fail("Result of parser should be a class!");
            }

        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException");
        } catch (Exception e) {
            e.printStackTrace();
            fail("Abstract tree creation failed!");
        }
    }

    public void writeClassfile(byte[] bytes) throws IOException {
        String className = new ClassReader(bytes).getClassName();
        File outputFile = new File(getInputFilePath(className + "_compare.class"));
        FileOutputStream output = new FileOutputStream(outputFile);
        output.write(bytes);
        output.close();
    }

    public InputStream disassembleClassFile(String fileName) throws FileNotFoundException {
        List<String> cmdList = new ArrayList<String>();
        cmdList.add("javap");
        cmdList.add("-c");
        cmdList.add(getInputFilePath(fileName));
        ProcessBuilder pb = new ProcessBuilder(cmdList);

        try {
            Process p = pb.start();
            p.waitFor();
            return p.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String compileAndDisassembleFileReference(String fileName) throws FileNotFoundException {
        compileReference(fileName + ".java");
        InputStream stream = disassembleClassFile(fileName + ".class");

        String output = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8)).lines()
                .collect(Collectors.joining("\n"));

        // Remove references to constant pool as they are implementation specific
        // This will replace constants with up to three digits with the correct
        // whitespace
        output = output.replaceAll("(?<=#\\d)\\d", " ");
        output = output.replaceAll("(?<=#\\d)\\d", " ");
        return output.replaceAll("#\\d", " ");
    }

    public String compileAndDisassembleFile(String fileName, scanner scanner) throws FileNotFoundException {
        compile(scanner, fileName + ".java");
        InputStream stream = disassembleClassFile(fileName + "_compare.class");

        String output = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8)).lines()
                .collect(Collectors.joining("\n"));

        // Remove references to constant pool as they are implementation specific
        // This will replace constants with up to three digits with the correct
        // whitespace
        output = output.replaceAll("(?<=#\\d)\\d", " ");
        output = output.replaceAll("(?<=#\\d)\\d", " ");
        return output.replaceAll("#\\d", " ");
    }
}
