import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class ThisTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_this/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    @Test
    @DisplayName("Read attribute with this")
    void this_attribute_read() throws FileNotFoundException {
        String filename = "this_attribute_read";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Write attribute with this")
    void this_attribute_write() throws FileNotFoundException {
        String filename = "this_attribute_write";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("This call to class function")
    void this_function_call() throws FileNotFoundException {
        String filename = "this_function";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }
}