class use_field_var {
    String a = "a";
    int b = 10;

    void test() {
        String c = a;
        int d = b;
    }
}
