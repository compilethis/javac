class math {
    int add(int a, int b) {
        return a + b;
    }

    int fakultaet_rek(int n) {
        int zeroValue = 0;
        if (n == zeroValue)
            return 1;

        if (n == 1)
            return 1;

        return n * fakultaet_rek(n - 1);
    }

    int fakultaet(int n) {
        int result = 1;
        int faktor = 1;
        while (faktor <= n) {
            result = result * faktor;
            faktor = faktor + 1;
        }
        return result;
    }

    boolean isPrime(int i) {
        if (i <= 1) {
            return false;
        }

        int j = 2;
        while (j <= i / 2) {
            int zeroValue = 0;
            if (modulo(i, j) == zeroValue) {
                return false;
            }
            j = j + 1;
        }

        return true;
    }

    int modulo(int a, int b) {
        int c = a - b * (a / b);
        return c;
    }

    int nextPrime(int num) {
        num = num + 1;
        int i = 2;
        while (i < num) {
            int zeroValue = 0;
            if (modulo(num, i) == zeroValue) {
                num = num + 1;
                i = 2;
            }
            i = i + 1;
        }
        return num;
    }

    int power(int a, int b) {
        int c = a;
        while (b > 1) {
            c = c * a;
            b = b - 1;
        }
        return c;
    }

    int root(int a) {
        int result = 0;
        while (a != result * result) {
            if (result == a) {
                return -1;
            }
            result = result + 1;
        }
        return result;
    }

    int abs(int a) {
        int zeroValue = 0;
        if (a < zeroValue) {
            a = a * -1;
        }
        return a;
    }

    boolean isBetween(int a, int b, int c) {
        if (a < b) {
            if (c > b) {
                return true;
            }
        }
        return false;
    }
}
