class Node {
    int data;
    Node leftNode;
    Node rightNode;

    Node(int data) {
        this.data = data;
        this.leftNode = null;
        this.rightNode = null;
    }

    void insert(Node node) {
        // defensive check
        if (node == null)
            return;

        if (node.data >= this.data) {
            if (this.rightNode != null) {
                this.rightNode.insert(node);
            } else {
                this.rightNode = node;
            }
        } else {
            if (this.leftNode != null) {
                this.leftNode.insert(node);
            } else {
                this.leftNode = node;
            }
        }
    }

    boolean search(int target) {
        if (this.data == target) {
            return true;
        }
        if (this.data < target) {
            return this.rightNode.search(target);
        } else {
            return this.leftNode.search(target);
        }
    }

    int findMin() {
        if (this.leftNode != null) {
            return this.leftNode.findMin();
        } else {
            return this.data;
        }
    }

    int findMax() {
        if (this.rightNode != null) {
            return this.rightNode.findMax();
        } else {
            return this.data;
        }
    }
}
