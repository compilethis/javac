import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class MethodTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_methods/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    // @Test
    // @DisplayName("Call built in method of string")
    // void call_builtin_method() throws FileNotFoundException {
    // String filename = "call_builtin_function";

    // String outputReference = helper.compileAndDisassembleFileReference(filename);
    // String output = helper.compileAndDisassembleFile(filename,
    // helper.createScanner(filename));

    // assertEquals(outputReference, output);
    // }

    @Test
    @DisplayName("Call method no arguments")
    void call_method_no_arguments() throws FileNotFoundException {
        String filename = "call_method_no_arguments";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Call method with arguments")
    void call_method_with_arguments() throws FileNotFoundException {
        String filename = "call_method_with_arguments";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Call method with return type and ignore it")
    void call_method_with_unused_return() throws FileNotFoundException {
        String filename = "call_method_ignore_return";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }
}