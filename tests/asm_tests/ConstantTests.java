import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class ConstantTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_constants/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    @Test
    @DisplayName("Compile load of small integer constant")
    void store_constant_small_integer() throws FileNotFoundException {
        String filename = "store_constant_small_integer";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile load of byte integer constant")
    void store_constant_byte_integer() throws FileNotFoundException {
        String filename = "store_constant_byte_integer";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile load of short integer constant")
    void store_constant_short_integer() throws FileNotFoundException {
        String filename = "store_constant_short_integer";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile load of long integer constant")
    void store_constant_long_integer() throws FileNotFoundException {
        String filename = "store_constant_long_integer";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile load of true boolean")
    void store_true_boolean() throws FileNotFoundException {
        String filename = "store_true_boolean";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile load of false boolean")
    void store_false_boolean() throws FileNotFoundException {
        String filename = "store_false_boolean";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile load of char")
    void store_char() throws FileNotFoundException {
        String filename = "store_char";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile load of string")
    void store_string() throws FileNotFoundException {
        String filename = "store_string";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile load of null value")
    void store_null() throws FileNotFoundException {
        String filename = "store_null";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }
}