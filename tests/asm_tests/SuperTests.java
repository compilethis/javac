import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class SuperTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_super/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    // @Test
    // @DisplayName("Super call to object class")
    // void super_object_class() throws FileNotFoundException {
    // String filename = "super_object_class";

    // String outputReference = helper.compileAndDisassembleFileReference(filename);
    // String output = helper.compileAndDisassembleFile(filename,
    // helper.createScanner(filename));

    // assertEquals(outputReference, output);
    // }
}