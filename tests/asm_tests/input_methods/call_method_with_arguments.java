class call_method_with_arguments {
    void test() {
        int a = add(1, 3);
    }

    int add(int a, int b) {
        return a + b;
    }
}
