class call_method_ignore_return {
    void test() {
        int i = test2();
        test2();
        test3();
    }

    int test2() {
        return 42;
    }

    void test3() {

    }
}
