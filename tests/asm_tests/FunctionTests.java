import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class FunctionTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_function/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    @Test
    @DisplayName("Compile empty void method without parameters")
    void compile_empty_void_method_no_parameters() throws FileNotFoundException {
        String filename = "empty_void_method_no_parameters";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile empty void method without parameters")
    void compile_empty_void_method_multiple_parameters() throws FileNotFoundException {
        String filename = "empty_void_method_multiple_parameters";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile multiple methods with different return types")
    void compile_empty_methods_with_return_type() throws FileNotFoundException {
        String filename = "empty_methods_with_return_type";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }
}
