import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class UnaryTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_unary/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    @Test
    @DisplayName("Use not unary operator")
    void not() throws FileNotFoundException {
        String filename = "not";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Use plus unary operator")
    void plus() throws FileNotFoundException {
        String filename = "plus";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Use minus unary operator")
    void minus() throws FileNotFoundException {
        String filename = "minus";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }
}