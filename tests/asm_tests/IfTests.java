import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class IfTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_if/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    @Test
    @DisplayName("Compile empty if")
    void compile_empty_if() throws FileNotFoundException {
        String filename = "empty_if";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile empty if else")
    void compile_empty_if_else() throws FileNotFoundException {
        String filename = "empty_if_else";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }
}
