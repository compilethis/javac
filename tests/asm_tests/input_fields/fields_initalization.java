class fields_initalization {
    String field1 = "field1";
    int field2 = 42;
    char field3;
    char field31;
    boolean field4 = false;
    String field5 = null;

    fields_initalization() {
        field2 = 43;
        field3 = 'c';
        field5 = "Hello World";
    }
}
