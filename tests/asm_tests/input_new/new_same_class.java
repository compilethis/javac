class new_same_class {
    new_same_class() {

    }

    new_same_class(int a, String b) {

    }

    new_same_class(String a) {

    }

    void test() {
        new_same_class a = new new_same_class();
        new_same_class b = new new_same_class(5, "Hello World");
        new_same_class c = new new_same_class(null);
    }
}
