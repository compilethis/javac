class empty_methods_with_return_type {
    void test1() {

    }

    char test2() {
        return 'a';
    }

    String test3() {
        return "abc";
    }

    int test4() {
        return 123;
    }

    boolean test5() {
        return true;
    }
}
