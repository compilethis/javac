import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class BooleanExpressionTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_boolean_expression/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    @Test
    @DisplayName("Compile Boolean expression with Binary less equals operator")
    void less_equals() throws FileNotFoundException {
        String filename = "less_equals";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile Boolean expression with Binary less than operator")
    void less_than() throws FileNotFoundException {
        String filename = "less_than";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile Boolean expression with Binary greater equals operator")
    void greater_equals() throws FileNotFoundException {
        String filename = "greater_equals";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile Boolean expression with Binary greater than operator")
    void greater_than() throws FileNotFoundException {
        String filename = "greater_than";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile Boolean expression with Binary equals operator")
    void equals() throws FileNotFoundException {
        String filename = "equals";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile Boolean expression with Binary not equals operator")
    void not_equals() throws FileNotFoundException {
        String filename = "not_equals";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile Boolean expression with Unary not operator")
    void not() throws FileNotFoundException {
        String filename = "not";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }
}
