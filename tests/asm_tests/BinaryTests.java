import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class BinaryTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_binary/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    @Test
    @DisplayName("Compile binary plus expression")
    void compile_binary_plus() throws FileNotFoundException {
        String filename = "binary_plus";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile binary minus expression")
    void compile_binary_minus() throws FileNotFoundException {
        String filename = "binary_minus";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile binary mul expression")
    void compile_binary_mul() throws FileNotFoundException {
        String filename = "binary_mul";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile binary div expression")
    void compile_binary_div() throws FileNotFoundException {
        String filename = "binary_div";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile binary and expression")
    void compile_binary_and() throws FileNotFoundException {
        String filename = "binary_and";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Compile binary or expression")
    void compile_binary_or() throws FileNotFoundException {
        String filename = "binary_or";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    // @Test
    // @DisplayName("Compile binary xor expression")
    // void compile_binary_xor() throws FileNotFoundException {
    // String filename = "binary_xor";

    // String outputReference = helper.compileAndDisassembleFileReference(filename);
    // String output = helper.compileAndDisassembleFile(filename,
    // helper.createScanner(filename));

    // assertEquals(outputReference, output);
    // }

    // @Test
    // @DisplayName("Compile binary shift left expression")
    // void compile_binary_shift_left() throws FileNotFoundException {
    // String filename = "binary_shift_left";

    // String outputReference = helper.compileAndDisassembleFileReference(filename);
    // String output = helper.compileAndDisassembleFile(filename,
    // helper.createScanner(filename));

    // assertEquals(outputReference, output);
    // }

    // @Test
    // @DisplayName("Compile binary shift right expression")
    // void compile_binary_shift_right() throws FileNotFoundException {
    // String filename = "binary_shift_right";

    // String outputReference = helper.compileAndDisassembleFileReference(filename);
    // String output = helper.compileAndDisassembleFile(filename,
    // helper.createScanner(filename));

    // assertEquals(outputReference, output);
    // }

    // @Test
    // @DisplayName("Compile binary unsigned shift right expression")
    // void compile_binary_unsigned_shift_right() throws FileNotFoundException {
    // String filename = "binary_unsigned_shift_right";

    // String outputReference = helper.compileAndDisassembleFileReference(filename);
    // String output = helper.compileAndDisassembleFile(filename,
    // helper.createScanner(filename));

    // assertEquals(outputReference, output);
    // }

    @Test
    @DisplayName("Compile binary less equals expression")
    void compile_binary_le() throws FileNotFoundException {
        String filename = "binary_le";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }
}
