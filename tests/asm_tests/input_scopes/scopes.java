class scopes {
    void test() {
        int i = 0;
        {
            int j = 0;
            {
                int h = 0;
            }
            int k = 0;
            j = 42;
        }
        int l = 0;
        i = 42;
    }
}
