import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class NewTests {
    private AsmTestHelper helper;
    private String input_folder = "/tests/asm_tests/input_new/";

    @BeforeEach
    public void setup() {
        helper = new AsmTestHelper(input_folder);
    }

    @Test
    @DisplayName("Create string with new")
    void new_string() throws FileNotFoundException {
        String filename = "new_string";

        String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        assertEquals(outputReference, output);
    }

    @Test
    @DisplayName("Create instance of same class with new")
    void new_same_class() throws FileNotFoundException {
        String filename = "new_same_class";

        // String outputReference = helper.compileAndDisassembleFileReference(filename);
        String output = helper.compileAndDisassembleFile(filename, helper.createScanner(filename));

        // assertEquals(outputReference, output);
    }
}