import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

public class MathTests {
    math mathInstance;

    @BeforeEach
    public void setup() {
        this.mathInstance = new math();
    }

    @Test
    @DisplayName("Self compiled add function test")
    void add() {
        int result = mathInstance.add(8, -13);

        assertEquals(result, -5);
    }

    @Test
    @DisplayName("Self compiled recursive faculty function test")
    void faculty_rek() {
        int result = mathInstance.fakultaet_rek(5);

        assertEquals(result, 120);
    }

    @Test
    @DisplayName("Self compiled recursive faculty function test")
    void faculty() {
        int result = mathInstance.fakultaet(3);

        assertEquals(result, 6);
    }

    @Test
    @DisplayName("Self compiled recursive faculty function test in a special case")
    void faculty_special_case() {
        int result = mathInstance.fakultaet(0);

        assertEquals(result, 1);
    }

    @Test
    @DisplayName("Self compiled is prime function test called with prime number")
    void is_prime_true() {
        boolean result = mathInstance.isPrime(43);

        assertEquals(result, true);
    }

    @Test
    @DisplayName("Self compiled is prime function test not called with prime number")
    void is_prime_false() {
        boolean result = mathInstance.isPrime(33);

        assertEquals(result, false);
    }

    @Test
    @DisplayName("Self compiled module function test no reminder")
    void modulo_no_reminder() {
        int result = mathInstance.modulo(27, 9);

        assertEquals(result, 0);
    }

    @Test
    @DisplayName("Self compiled module function test no reminder")
    void modulo_reminder() {
        int result = mathInstance.modulo(29, 9);

        assertEquals(result, 2);
    }

    @Test
    @DisplayName("Self compiled next prime function")
    void next_prime() {
        int result = mathInstance.nextPrime(5);

        assertEquals(result, 7);
    }

    @Test
    @DisplayName("Self compiled power function")
    void power() {
        int result = mathInstance.power(4, 3);

        assertEquals(result, 64);
    }

    @Test
    @DisplayName("Self compiled root function")
    void root() {
        int result = mathInstance.root(100);

        assertEquals(result, 10);
    }

    @Test
    @DisplayName("Self compiled abs function")
    void abs() {
        int result = mathInstance.abs(-10);

        assertEquals(result, 10);
    }

    @Test
    @DisplayName("Self compiled is between function true")
    void isBetween_true() {
        boolean result = mathInstance.isBetween(1, 5, 10);

        assertEquals(result, true);
    }

    @Test
    @DisplayName("Self compiled is between function false")
    void isBetween_false() {
        boolean result = mathInstance.isBetween(10, 20, 10);

        assertEquals(result, false);
    }
}
