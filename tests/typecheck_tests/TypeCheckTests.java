import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.*;

public class TypeCheckTests {
    private javaparser parser;
    private String INPUT_FOLDER = "/tests/typecheck_tests/input/";

    @BeforeEach
    public void setup() {
        parser = new javaparser();
    }

    public scanner createScanner(String filename) throws FileNotFoundException {
        String filePath = new File("").getAbsolutePath();

        InputStream inputStream = new FileInputStream(filePath.concat(filename));
        return new scanner(new java.io.InputStreamReader(inputStream));
    }

    public void parseAndCheck(scanner scanner) throws IOException, javaparser.yyException {
        Object result = parser.yyparse(scanner);
        if (result instanceof CLASS) {
            CLASS resultClass = (CLASS) result;
            resultClass.typeCheck();
        } else {
            fail("Result of parser should be a class!");
        }
    }

    @Test
    @DisplayName("Empty file allowed")
    void empty_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "empty.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("Class name with dollar signs allowed")
    void empty_class_with_dollar_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "empty_class_with_underscore.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("Class name with underscores allowed")
    void empty_class_with_underscore_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "empty_class_with_dollar_sign.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("Class name with numbers allowed")
    void empty_class_with_number_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "empty_class_with_number.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("Class name with beginning number not allowed")
    void empty_class_with_beginning_number_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "empty_class_with_beginning_number.java");

        assertThrows(javaparser.yyException.class, () -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("Class name with only one number not allowed")
    void empty_class_with_only_number_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "empty_class_with_only_number.java");

        assertThrows(javaparser.yyException.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("random")
    void random() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "random.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("Variable not Declaired")
    void var_not_declared_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "var_not_declared.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("inst var acessible")
    void inst_var_acessible_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "inst_var_acessible.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("method call outside of executable Block")
    void methodcall_outside_executable_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "methodcall_outside_executable.java");

        assertThrows(javaparser.yyException.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("Var out of Scope")
    void var_not_in_scope_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "var_not_in_scope.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("Var out of Scope 2")
    void var_not_in_scope_2_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "var_not_in_scope_2.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("Illegal String Comparison <")
    void illegal_string_comparison_smaller_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "illegal_string_comparison_smaller.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("Illegal String Comparison >")
    void illegal_string_comparison_bigger_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "illegal_string_comparison_bigger.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("Illegal String Comparison >=")
    void illegal_string_comparison_bigger_equal_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "illegal_string_comparison_bigger_equal.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("Illegal String Comparison <=")
    void illegal_string_comparison_smaller_equal_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "illegal_string_comparison_smaller_equal_bigger.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("String Operation subtract throws")
    void llegal_sring_operation_subtract_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "illegal_sring_operation_subtract.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("String Operation divide throws")
    void illegal_string_operation_divide_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "illegal_string_operation_divide.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("String Operation multiply throws")
    void illegal_string_operation_multiply_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "illegal_string_operation_multiply.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("Var name equals class name throws")
    void var_name_equals_class_name_throws() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "var_name_equals_class_name.java");

        assertThrows(java.lang.Error.class, () -> {
            parseAndCheck(scanner);
        });
    }
    @Test
    @DisplayName("New class from same type")
    void new_class_from_same_type_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "new_class_from_same_type.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }
}
