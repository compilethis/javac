import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.*;

public class ScannerTests {
    private javaparser parser;
    private String INPUT_FOLDER = "/tests/scanner_tests/input/";

    @BeforeEach
    public void setup() {
        parser = new javaparser();
    }

    public scanner createScanner(String filename) throws FileNotFoundException {
        String filePath = new File("").getAbsolutePath();

        InputStream inputStream = new FileInputStream(filePath.concat(filename));
        return new scanner(new java.io.InputStreamReader(inputStream));
    }

    public void parseAndCheck(scanner scanner) throws IOException, javaparser.yyException {
        Object result = parser.yyparse(scanner);
        if (result instanceof CLASS) {
            CLASS resultClass = (CLASS) result;
            resultClass.typeCheck();
        } else {
            fail("Result of parser should be a class!");
        }
    }

    @Test
    @DisplayName("Comment Single Line")
    void math_simple_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "CommentSingleLine.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }
}
