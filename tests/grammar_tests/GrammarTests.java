import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.*;

public class GrammarTests {
    private javaparser parser;
    private String INPUT_FOLDER = "/tests/grammar_tests/input/";

    @BeforeEach
    public void setup() {
        parser = new javaparser();
    }

    public scanner createScanner(String filename) throws FileNotFoundException {
        String filePath = new File("").getAbsolutePath();

        InputStream inputStream = new FileInputStream(filePath.concat(filename));
        return new scanner(new java.io.InputStreamReader(inputStream));
    }

    public void parseAndCheck(scanner scanner) throws IOException, javaparser.yyException {
        Object result = parser.yyparse(scanner);
        if (result instanceof CLASS) {
            CLASS resultClass = (CLASS) result;
            resultClass.typeCheck();
        } else {
            fail("Result of parser should be a class!");
        }
    }

    @Test
    @DisplayName("Math Simple")
    void math_simple_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "MathSimple.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("Math Brackets")
    void math_brackets_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "MathBrackets.java");

        assertDoesNotThrow(() -> {
            parseAndCheck(scanner);
        });
    }

    @Test
    @DisplayName("Math Order Manual")
    void math_order_manual_throws_no_error() throws FileNotFoundException {
        scanner scanner = createScanner(INPUT_FOLDER + "order_tests/dot_before_dash.java");
        assertDoesNotThrow(() -> {
		parseAndCheckValue(scanner,17);
        });
    }

    public void parseAndCheckValue(scanner scanner, int value) throws IOException, javaparser.yyException {
        Object result = parser.yyparse(scanner);
        if (result instanceof CLASS) {
            CLASS resultClass = (CLASS) result;
	    System.out.println(resultClass.getDOTRepresentation());
	}
    }
}
