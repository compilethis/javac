import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.objectweb.asm.*;

public class Main {
        public static void main(String[] args) {
                scanner scanner = new scanner(new java.io.InputStreamReader(System.in));
                javaparser parser = new javaparser();
                try {
                        Object result = parser.yyparse(scanner);
                        if (result instanceof CLASS) {
                                CLASS resultClass = (CLASS) result;
                                resultClass.typeCheckAll();
                                byte[] bytes = resultClass.codeGen(resultClass.name);
                                writeClassfile(bytes);
                        } else {
                                System.err.print("Result of parser should be a class!");
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }

        static void writeClassfile(byte[] bytes) throws IOException {
                String className = new ClassReader(bytes).getClassName();

                System.out.println(className);
                File outputFile = new File("build/output/" + className + ".class");
                outputFile.getParentFile().mkdirs();
                FileOutputStream output = new FileOutputStream(outputFile);
                output.write(bytes);
                output.close();
        }
}
